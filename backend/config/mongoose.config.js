const mongoose = require('mongoose');
const DB_URI = process.env.MONGODB_URI;

/** MongoDB database connection */
mongoose.connect(DB_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  });

  const connection = mongoose.connection;
  
  connection.once("open", () => {
    console.log(`Successfully connect to mongodb at ${DB_URI}`);
  });