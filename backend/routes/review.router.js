const router = require('express').Router();
const App = require('../models/appdata.model');
const Review = require('../models/review.model');
const { body, validationResult } = require('express-validator');
const gplay = require('google-play-scraper');
const { Container } = require('@nlpjs/core');
const { SentimentAnalyzer } = require('@nlpjs/sentiment');
const { LangId } = require('@nlpjs/lang-id');
const { Parser } = require('json2csv');


router.get('/', (req, res) => {
    App.find()
    .then(results => {
        res.status(200).json({
            'msg': 'Scrapping datas',
            'data': results
        });
    }).catch(err => {
        res.status(500).json({'msg': err});
    });
});

router.post('/',
    body('app_id').notEmpty(),
    body('num').notEmpty().isNumeric(),
    body('sort').notEmpty(),
(req, res) => {
    
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    
    let sort;
    let sortText;
    let appId = req.body.app_id;
    let num = req.body.num;
    const lang = "id";
    const country = "ID";
    switch (req.body.sort) {
        case 'RATING':
            sort = gplay.sort.RATING;
            sortText = "RATING";
            break;
        case 'NEWEST':
            sort = gplay.sort.NEWEST;
            sortText = "NEWEST";
            break;
        case 'HELPFULNESS':
            sort = gplay.sort.HELPFULNESS;
            sortText = "HELPFULNESS";
            break;
        default:
            sort = gplay.sort.NEWEST;
            sortText = "NEWEST";
            break;
    }

    const appMaster = new App({
        app_id: appId,
        country: country,
        lang: lang,
        sort: sortText
    });

    appMaster.save()
        .then(() => {
            gplay.reviews({
                appId: appId,
                sort: sort,
                num: num,
                lang: lang,
                country: country
            }).then(results => {
                results.data.map(data => {
                    const container = new Container();
                    container.use(LangId);
                    const sentiment = new SentimentAnalyzer({container});
                    sentiment.process({
                        locale: 'id',
                        text: data.text,
                    }).then(sentimentResult => {
                        let newReview = new Review({
                            app_id: appMaster._id,
                            user_name: data.userName,
                            user_image: data.userImage,
                            date: data.date,
                            score: data.score,
                            text: data.text,
                            sentiment_analysis:[
                                {
                                    score: sentimentResult.sentiment.score,
                                    num_words: sentimentResult.sentiment.numWords,
                                    num_hits: sentimentResult.sentiment.numHits,
                                    average: sentimentResult.sentiment.average,
                                    type_dict: sentimentResult.sentiment.type,
                                    locale: sentimentResult.sentiment.locale,
                                    vote: sentimentResult.sentiment.vote,
                                }
                            ]
                        });
                        newReview.save()
                            .then(() => {})
                            .catch(err => {
                                res.status(500).json({'msg': err});
                            });
                    }).catch(err => {
                        res.status(500).json({'msg': err});
                    });
                });
                res.status(200).json({
                    'msg': 'Successfully get data!'
                });
            }).catch(err => {
                res.status(500).json({
                    'msg': err
                });
            });
        }).catch(err => {
            res.status(500).json({
                'msg': err
            });
        });
});

router.get('/:id', (req, res) => {
    const id = req.params.id;
    Review.find({'app_id': id})
    .then(results => {
        res.status(200).json({'msg': 'Review data', 'data': results});
    }).catch(err => {
        res.status(500).json({'msg': err});
    });
});

router.delete('/:id', (req, res) => {
    let id = req.params.id;
    Review.deleteMany({
            "app_id": id
        })
        .then(() => {
            App.findByIdAndDelete(id)
                .then(() => {
                    res.status(200).json({'msg': 'App and review data deleted!'});
                }).catch(err => {
                    res.status(500).json({'msg': err});
                });
        }).catch(err => {
            res.status(500).json({'msg': err});
        });
});

router.delete('/review/:id', (req, res) => {
    let id = req.params.id;
    Review.deleteOne({'_id': id})
    .then(() => {
        res.status(200).json({'msg': 'Review deleted!'});
    }).catch(err => {
        res.status(500).json({'msg': err});
    })
});

router.get('/review/detail/:id', (req, res) => {
    let id = req.params.id;
    Review.findOne({'_id': id})
    .then(results => {
        res.status(200).json({'msg': 'Detail sentiment for review', 'data': results.sentiment_analysis[0]});
    }).catch(err => {
        res.status(500).json({'msg': err});
    })
});

router.get('/export/csv/:id', (req, res) => {
    const id = req.params.id;
    App.findById(id).then(details => {
        Review.find({'app_id': id})
        .then(reviews => {
            const fields = ['_id', 'user_name', 'user_image', 'date', 'score', 'text', 
            'sentiment_analysis[0]["score"]', 'sentiment_analysis[0]["num_words"]', 'sentiment_analysis[0]["num_hits"]',
            'sentiment_analysis[0]["average"]', 'sentiment_analysis[0]["type_dict"]', 'sentiment_analysis[0]["locale"]',
            'sentiment_analysis[0]["vote"]', 
        ];
            const opts = { fields };
            const fileName = details.app_id + "-" + "reviews.csv";
            try {
                const parser = new Parser(opts);
                const csv = parser.parse(reviews);
                res.attachment(fileName);
                res.status(200).send(csv);
            } catch (error) {
                console.log(error);
            }
        }).catch(err => {
            res.status(500).json({'msg': err});
        });
    }).catch(err => {
        res.status(500).json({'msg': err});
    });
});

module.exports = router;