const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reviewShema = new Schema({
    app_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Masterdata'
    },
    user_name:{
        type: String, 
        required: true,
    },
    user_image:{
        type: String, 
        required: true,
    },
    score:{
        type: Number, 
        required: true,
    },
    date:{
        type: String, 
        required: true,
    },
    text:{
        type: String, 
        required: true,
    },
    sentiment_analysis:[],
}, {
    timestamps:true
});

const Review = mongoose.model('Review', reviewShema);
module.exports = Review;