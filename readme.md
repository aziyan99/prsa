### PRSA (Play Store Sentiment Analysis)
This web is used for get and analyze comments from playstore like apps games and etc. To use this website all you need is the app id(example: us.zoom.videomeetings for zoom video meeting app), count (example: 500), and what type of sorting you need (NEWEST, RATING, and HELPFULNESS). This web also can export the result to the csv file. For the infrastructure at frontend this website using NuxtJs (available at [https://id.nuxtjs.org](https://id.nuxtjs.org)) and for the backend using ExpressJs (avaiable at [https://expressjs.com](https://expressjs.com)), for storing data this web using NoSQL database named MongoDB by taking advantage of the free services of the MongoDB Atlas (available at [https://www.mongodb.com/cloud/atlas](https://www.mongodb.com/cloud/atlas)).

### How it works
In the nutshell this web working simple, this web using package named **google-play-scraper** (available at [https://www.npmjs.com/package/google-play-scraper](https://www.npmjs.com/package/google-play-scraper)) for grabbing the apps or games comments from playstore, and after that the comments will be process to analyze the sentiments with package named **nlpjs** (available at [https://github.com/axa-group/nlp.js](https://github.com/axa-group/nlp.js)).

### Installation
#### Frontend
1. Make sure you have NodeJs and NPM installed on your machine.
2. Make sure you have installed yarn Globally. Please refer to [https://classic.yarnpkg.com/en/docs/install/#mac-stable](https://classic.yarnpkg.com/en/docs/install/#mac-stable).
3. Run `yarn install` to install the depedencies.
4. Run `yarn dev` to start the website.
5. Default host will start at `http://localhost:82`, if your facing error, like port has been used feel free to change the **port** at `nuxt.config.js` file in the `frontend` folder.
#### Backend
1. Make sure you have NodeJs and NPM installed on your machine.
2. Navigate to the `backend` folder from terminal `cd backend`.
3. Run `npm install` and wait till end.
4. To run the server run command `npm run dev` or `npm run start`.
5. The default host will start at `http://localhost:81`, if your facing error, like port has been used feel free to change the **port** at `.env` file in the `backend` folder.

### In The End (like: linkin park song :v)
For any questions or sharing feel free to reach me at `rajaazian08@gmail.com`.

> Thanks you :).