const express = require('express');
require('dotenv').config();
const cors = require('cors');
require('./config/mongoose.config');

const masterDataRouter = require('./routes/review.router');

const PORT = process.env.PORT || 81;
const app = express();
app.use(cors());
app.use(express.json());

app.get('/', (req, res) => {
    res.status('200').json({
        'msg': 'Hello there',
        'avaliable_enpoint': {
            '/reviews':{
                'GET':{
                    'method': 'GET',
                    'desc': 'Get all saved scrapping data'
                },
                'POST':{
                    'method': 'POST',
                    'desc': 'Scrapping new data',
                    'required':{
                        'app_id':{
                            'type': 'String',
                            'desc': 'The application ID'
                        },
                        'num':{
                            'type': 'Number',
                            'desc': 'Num of data that wanna to scrapping'
                        },
                        'sort': {
                            'type': 'Enum(NEWEST, RATING, HELPFULNESS, default=NEWEST)',
                            'desc': 'Priority of scrapping data'
                        }
                    }
                }
            },
            '/reviews/:id':{
                'GET':{
                    'method': 'GET',
                    'desc': 'Get all reviews of scrapping data',
                    'params':{
                        'id':{
                            'type': 'String',
                            'desc': 'Object ID off application ID'
                        }
                    }
                },
                'DELETE':{
                    'method': 'DELETE',
                    'desc': 'Delete application data with the scrapping data',
                    'params':{
                        'id':{
                            'type': 'String',
                            'desc': 'Object ID off application ID'
                        }
                    }
                }
            },
            '/reviews/review/:id':{
                'DELETE':{
                    'method': 'DELETE',
                    'desc': 'Delete one of application review',
                    'params':{
                        'id':{
                            'type': 'String',
                            'desc': 'Object ID off review ID'
                        }
                    }
                }
            },
            '/reviews/review/detail/:id':{
                'GET':{
                    'method': 'GET',
                    'desc': 'Get detailed of sentiment analysis for one review',
                    'params':{
                        'id':{
                            'type': 'String',
                            'desc': 'Object ID off review ID'
                        }
                    }
                }
            },
            '/reviews/export/csv/:id':{
                'GET':{
                    'method': 'GET',
                    'desc': 'Export all reviews data from one application to csv',
                    'params':{
                        'id':{
                            'type': 'String',
                            'desc': 'Object ID off application ID'
                        }
                    }
                }
            }
        }
    });
});

app.use('/reviews', masterDataRouter);

/** 404 Route */
app.use("*", (req, res) => {
    res.status(404).json({'msg': 'Resource not found'});
});

app.listen(PORT, () => {
    console.log(`Server up and running on port ${PORT}`);
});
