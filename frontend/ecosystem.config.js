module.exports = {
  apps: [
    {
	    name: 'client-apps',
       exec_mode: 'cluster',
       instance: 'max',
       script: './node_modules/nuxt/bin/nuxt.js',
       args: 'start'
    }
  ]
}
