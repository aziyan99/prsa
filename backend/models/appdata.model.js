const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const appSchema = new Schema({
    app_id:{
        type: String,
        required: true,
    },
    country:{
        type: String,
        required: true,
    },
    lang:{
        type: String,
        required: true,
    },
    sort:{
        type: String,
        required: true,
    }
}, {
    timestamps:true
});

const App = mongoose.model('Masterdata', appSchema);
module.exports = App;